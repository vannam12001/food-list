import { Food } from "../../models/v1/model.js";
export let renderFoodList = (foodArr) => {
  let contentHTML = "";
  foodArr.forEach((item) => {
    let { maMon, tenMon, loai, khuyenMai, tinhTrang, tinhGiaKM, giaMon } = item;
    let contentTr = `<tr>
    <td>${maMon}</td>
    <td>${tenMon}</td>
    <td>${loai ? "Chay" : "Mặn"}</td>
    <td>${giaMon}</td>
    <td>${khuyenMai}</td>
    <td>${item.tinhGiaKM()}</td>
    <td>${tinhTrang ? "Còn" : "Hết"}</td>
    <td>
    <button onclick="suaMonAn(${maMon})" class ="btn btn-primary">Edit</button>
    <button onclick="xoaMonAn(${maMon})" class="btn btn-danger">Delete</button>
    </td>
    </tr>`;
    contentHTML = contentHTML + contentTr;

    // console.log(item);
  });
  document.getElementById("tbodyFood").innerHTML = contentHTML;
};
